jam-lib (0.1.git20240425.aec45be+dfsg-1) unstable; urgency=medium

  * New upstream version 0.1.git20240425.aec45be+dfsg
  * remove-mac-only-code.patch: refresh.
  * d/control: declare compliance to standards version 4.7.0.
  * d/compat: deleted; d/control: bump to debhelper compat 13.
  * d/libjam-java-doc.javadoc: fix lintian error in doc-base.
  * remove-mac-only-code.patch: declare forwarding not-needed.
    The change brought by the patch is necessary due to files excluded by
    the packaging, thus specific to Debian.
  * javadoc.patch: dep3 header with forwarding upstream.
  * d/control: d/rules does not require root.
  * d/control: libjam-java-doc depends on libjs-jquery and libjs-jquery-ui.
  * d/libjam-java-doc.links: use libjs-* scripts instead of vendored ones.
  * d/clean: new: cleanup build-java9-only/ directory. (Closes: #1046367)
  * d/control: add myself to uploaders.

 -- Étienne Mollier <emollier@debian.org>  Fri, 11 Oct 2024 12:10:48 +0200

jam-lib (0.1.git20180106.740247a+dfsg-1) unstable; urgency=medium

  * Use git mode in watch file
  * Drop unneeded get-orig-source script
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.3.0

 -- Andreas Tille <tille@debian.org>  Tue, 08 Jan 2019 13:55:01 +0100

jam-lib (0.1.20140824-2) unstable; urgency=medium

  * Fix homepage
  * update d/copyright
  * same license for code and packaging
  * cme fix dpkg-control
  * fake watch file
  * debhelper 10

 -- Andreas Tille <tille@debian.org>  Wed, 11 Jan 2017 21:09:18 +0100

jam-lib (0.1.20140824-1) unstable; urgency=medium

  * New Git checkout featuring functionality used in beast-mcmc release
  * cme fix dpkg-control
  * DEP3 headers for patches
  * Moved from SVN to Git

 -- Andreas Tille <tille@debian.org>  Sun, 19 Jul 2015 16:56:37 +0200

jam-lib (0.0.r304-1) unstable; urgency=low

  * New upstream version
  * debian/get-orig-source: Use xz compression
  * debian/control:
     - Standards-Version: 3.9.4 (no changes needed)
     - Remove DM-Upload-Allowed
     - debhelper 9
     - cme fix dpkg-control
  * debian/copyright: cme fix dpkg-copyright

 -- Andreas Tille <tille@debian.org>  Thu, 27 Jun 2013 18:33:37 +0200

jam-lib (0.0.r297-1) unstable; urgency=low

  * Initial Debian Upload (Closes: #612953)

 -- Andreas Tille <tille@debian.org>  Thu, 10 Feb 2011 10:26:38 +0100
